package zad5;

public class TimerRunnable implements Runnable {

    private int timeTodie = 0;

    public void setTimeTodie(int timeTodie) {
        this.timeTodie = timeTodie;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(timeTodie);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("To Twój timer " + timeTodie / 1000 + " s. - wake up!");
    }
}
