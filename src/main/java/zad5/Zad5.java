package zad5;

/*
Stwórz aplikację "timery". Wątek główny przyjmuje w linii liczbę. Liczba oznacza czas do wybudzenia.
Po wpisaniu liczby (np. 50000) aplikacja ma startować nowy wątek, który ma się wybudzić za ten czas (np. za 50 s.)
 i wypisze komunikat: (To Twój timer 50 s. - wake up!)
*/


import java.util.Scanner;

public class Zad5 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int timer = 0;
        System.out.println("Wpisz wartosc timera: ");


        while (true) {

            TimerRunnable runnable = new TimerRunnable();
            timer = scanner.nextInt() * 1000;
            runnable.setTimeTodie(timer);
            Thread thread = new Thread(runnable);
            thread.start();


            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
