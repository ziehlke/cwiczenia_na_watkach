package zad3;

import java.io.File;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class FileAddressRunnable implements Runnable {

    private String address = ".";
    private Set<File> files = new TreeSet<>();

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public void run() {
        File directory = new File(address);

        files.addAll(Arrays.asList(directory.listFiles()));
        Set<File> filesCopy = new TreeSet<>();
        Set<File> newFiles = new TreeSet<>();

        while (true) {

            directory = new File(address); // in case of new directory input
            newFiles.addAll(Arrays.asList(directory.listFiles()));
            filesCopy.addAll(files);
            //
            files.removeAll(newFiles);
            newFiles.removeAll(filesCopy);

            if (!files.isEmpty()) {
                System.out.println("files deleted: " + files);
            }

            if (!newFiles.isEmpty()) {
                System.out.println("new files: " + newFiles);
            }

            newFiles.clear();
            filesCopy.clear();
            files.clear();
            files.addAll(Arrays.asList(directory.listFiles()));

            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
