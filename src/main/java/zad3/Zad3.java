package zad3;

/*
Stwórz aplikację która co sekundę (w drugim wątku) sprawdza czy w katalogu pojawił się nowy plik.
Jeśli pojawił się nowy plik lub jakiś zniknął, to wypisuje stosowny komunikat na ekran. Drugi wątek (główny)
 ma przyjmować w linii poleceń adres katalogu który jest sprawdzany (możemy zmienic adres czytanego katalogu).
*/


import java.io.File;
import java.util.Scanner;

public class Zad3 {


    public static void main(String[] args) {
        FileAddressRunnable runnable = new FileAddressRunnable();
        Thread thread = new Thread(runnable);
        Scanner scanner = new Scanner(System.in);
        String address = ".";
        thread.start();

        System.out.println("Type in directory address to track: ");

        while (true) {

            address = scanner.nextLine();
            File file = new File(address);
            if (file.isDirectory()) {
                System.out.println("...tracking: " + file.getName());
                runnable.setAddress(address);
            } else System.out.println("given adress is not a directory!");


            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
