package zad4;

/*
Stwórz aplikację która cyklicznie czyta plik i jeśli doszła jakakolwiek zmiana w pliku, to wypisuje ją na ekran.
Drugi wątek ma przyjmować adres pliku który jest sprawdzany (możemy zmienić adres czytanego pliku.
*/

import java.io.File;
import java.util.Scanner;

public class Zad4 {


    public static void main(String[] args) {
        String address = "";
        File file;
        FileCheckerRunnable runnable = new FileCheckerRunnable();
        Thread thread = new Thread(runnable);
        Scanner scanner = new Scanner(System.in);

        thread.start();

        while (true) {

            address = scanner.nextLine();
            file = new File(address);

            if (file.isFile()) {
                runnable.setAddress(address);
                System.out.println("...monitoring file: " + file.getName());
            } else System.out.println("there's no file at given address.");


            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
