package zad4;

import java.io.File;

public class FileCheckerRunnable implements Runnable {

    private String address = "plik1.txt";


    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public void run() {
        File file = new File(address);

        long copyLenght = file.length();
        long copyModyfication = file.lastModified();


        while (true) {

            file = new File(address);
            if (!file.exists()) {
                System.out.println("The file has been deleted.");
            } else {
                file = new File(address);
//                System.out.println("monitoring file... " + file.getName());
            }


            if (file.length() != copyLenght) {
                System.out.println("The file changed it's size.");
                copyLenght = file.length();
            }
            if (file.lastModified() != copyModyfication) {
                System.out.println("The file modification date has changed. ");
                copyModyfication = file.lastModified();
            }


            try {
                Thread.sleep(900);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
