package zad2;

/*
Stwórz aplikację a w niej wątek który raz na 30 sekund wypisuje na ekran:

1. Hello World! - tekst
2. Hello World!! - tekst
3. Hello World!!! - tekst

Główny wątek powinien przyjmować tekst który ma występować po hello world. np. jeśli wpisze:
"Michałki"

wątek powinien wypisywać:
...
4. Hello World!!!! - Michałki
...
*/

import java.util.Scanner;

public class Zad2 {

    public static void main(String[] args) {

        TextRunnable runnable = new TextRunnable();
        Thread thread = new Thread(runnable);
        thread.start();


        System.out.println("Wpisz text do wyświetlenia: ");

        while (true) {

        Scanner scanner = new Scanner(System.in);
        runnable.setText(" - " + scanner.nextLine());
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
