package zad1;



/*
Stwórz aplikację, w niej uruchom wątek, którego zadaniem jest po 30 sekundach wypisać komunikat "Juhuuuu!".
Po tej czynności aplikacja ma się zamknąć.
*/


public class Zad1 {

    public static void main(String[] args) {

        Runnable runnable = () -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Juhuuuuuu!");
        };

        Thread thread = new Thread(runnable);


        thread.start();



    }
}
